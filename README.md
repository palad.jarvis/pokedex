# Pokedex (pokedex)

Pokemon lists

## Prerequisites
You will need [Node.js](https://nodejs.org/en/) version 12.0 or greater installed on your system.

## Setup

Get the code by cloning this repository using git
```bash
git clone https://gitlab.com/palad.jarvis/pokedex.git
```

### Install Quasar CLI
```bash
npm install -g @quasar/cli 
```

### or

```bash
yarn global add @quasar/cli
```

### Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Run the test
```bash
npm run test:unit
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v1.quasar.dev/quasar-cli/quasar-conf-js).
