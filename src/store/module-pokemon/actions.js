import axios from "axios"
import {api} from "src/boot/axios"
export async function fetchData(context,payload) {

    try {

        //end point for scrolling / pagination api call
        
        let apiEndpoint = !context.state.pokemons.length ? '/pokemon?limit=8' : payload?.url ?? context.state.urlApi


        const pokemonsLists = await api.get(apiEndpoint || context.state.urlApi);
    
        context.commit('setMeta', {
            url: pokemonsLists.data.next,
            totalPokemons: payload?.url ? 0 : pokemonsLists.data.count
        })
    
    

        let lists = pokemonsLists.data.results;

        if(payload?.type) context.commit('clearPokemon')
    
        //fetch the details of specific pokemon
        await lists.map(async (item) => {
            await context.dispatch('setData', item)

        });
    
        return
    }

    catch (err) {
     
        if(err?.response?.status === 404) {
            context.commit('clearPokemon');
        }

    }
}

export async function setData(context,payload) {

    try {
        const pokemon = await axios.get(`https://pokeapi.co/api/v2/pokemon/${payload.name}`);
    
        const { name, id, sprites, types } = pokemon.data;
    
        const pokeData = {
            image:
                sprites.other?.dream_world?.front_default ??
                sprites.other["official-artwork"].front_default ??
                sprites.front_default,
            name,
            id,
            types,
        };
    
        if(!payload.show) context.commit('setPokemon', pokeData);
        return pokemon
    }

    catch (err) {

        if(err?.response?.status === 404) {
            return err.response
        }              
    }

}

