export function pokemons (state) {
    let pokemons = [...state.pokemons]
    return pokemons.sort(
        (firstItem, secondItem) => firstItem.id - secondItem.id
    );
}


export function loading(state) {
    return state.loading
}

export function urlApi(state) {
    return state.urlApi
}

export function totalPokemons(state) {
    return state.totalPokemons
}

export function search(state) {
    return state.search
}