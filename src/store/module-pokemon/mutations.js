

export function setPokemon(state,payload) {
    state.pokemons = [...state.pokemons, payload];
}


export function setMeta(state,payload) {
    state.urlApi = payload.url;
    state.totalPokemons = payload.totalPokemons;
}

export function setUrl(state,payload) {
    state.urlApi = payload
}

export function toggleLoading(state) {
    state.loading = !state.loading
}

export function clearPokemon(state) {
    state.pokemons = []
}





export function updateSearch(state,payload) {
    state.search = payload;
}