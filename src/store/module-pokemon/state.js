export default function () {
  return {
    pokemons: [],
    loading: false,
    urlApi: "https://pokeapi.co/api/v2/pokemon?limit=8",
    totalPokemons: 0,
    search: null
  }
}
