// import something here

import VueEllipseProgress from 'vue-ellipse-progress';
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'
// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files

export default async ({Vue}) => {
  Vue.use(VueEllipseProgress)
  Vue.use(VueAwesomeSwiper)
  // something to do
}
