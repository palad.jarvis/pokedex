import Vue from "vue";
import axios from "axios";

Vue.prototype.$axios = axios;

const api = axios.create({
  baseURL: "https://pokeapi.co/api/v2/"
});

Vue.prototype.$api = api;

export { api };
