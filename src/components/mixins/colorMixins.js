export const typeColor = {
  methods: {
    getTypeColor(payload) {
      let type = payload.toLowerCase();
      switch (type) {
        case "normal":
          return "brown-7";
        case "fire":
          return "orange-7";
        case "water":
          return "light-blue-8";
        case "grass":
          return "green-9";
        case "electric":
          return "yellow-9";
        case "ice":
          return "teal-12";
        case "fighting":
          return "red-14";
        case "poison":
          return "deep-purple-13";
        case "ground":
          return "light-green-8";
        case "flying":
          return "purple-5";
        case "psychic":
          return "pink-12";
        case "bug":
          return "lime-8";
        case "rock":
          return "blue-grey-8";
        case "ghost":
          return "grey-7";
        case "dark":
          return "grey-10";
        case "dragon":
          return "indigo-9";
        case "steel":
          return "blue-grey-3";
        case "fairy":
          return "pink-4";
      }
    },

    getStatColor(stat) {
      switch(stat) {
        case 'hp':
          return '#25c450'
        case 'attack':
          return '#d61818'
        case 'defense':
          return '#1739e6'
        case 'special-attack':
          return '#820307'
        case 'special-defense':
          return '#190569'
        case 'speed':
          return '#fcf10f'
      }
    }
  }
};
