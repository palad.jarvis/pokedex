export const routeManage = {
    methods: {
        routerPush(url) {
            if (this.$route.path !== url) this.$router.push(url);
          },

        routerReplace(from, to) {
            if (from === to) return;
            this.$router.replace({ name: to });
          }
    }
}