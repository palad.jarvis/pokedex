import { mount, createLocalVue, shallowMount } from '@vue/test-utils';
import Vuex from 'vuex'
import IndexPage from "src/pages/Index.vue";
import ShowPage from "src/pages/pokemon/show.vue";
import MainLayout from "src/layouts/MainLayout.vue";
import * as All from 'quasar';
import pokemon from "src/store/module-pokemon"
import flushPromises from "flush-promises";
import { routeManage } from "src/components/mixins/commonMixins";

const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});




const $router = {
    push: jest.fn()
};

const $route = {
    params: {
        name: 'pikachu'
    }
}


describe('Index.vue', () => {


    const localVue = createLocalVue();
    
    localVue.use(Vuex);
    localVue.use(Quasar, { components });
 
    let getters
    let store
    let actions

    beforeEach( () => {




        actions = {
            fetchData: jest.fn(),
            setData: () => { return Promise.resolve({
                status: 200
            })}
        }


        getters = {
            pokemons: () => pokemon.getters.pokemons,
            loading: () => false,
            search: () => 'pikachu'
        }

        store = new Vuex.Store({
            modules: {
                pokemon: {
                    getters,
                    actions,
                    mutations: pokemon.mutations,
                    namespaced: true
                }
            }

        })


        
    })

 


    it('start on pagination view', () => {
        
        
        const wrapper = mount(IndexPage, {
            mixins: [routeManage],
            store, localVue,

        });
        const vm = wrapper.vm;   
        
        expect(vm.viewingState).toContain('pagination')
    })




    it('view should be on scroll mode when scroll button is pressed',async () => {
        const wrapper = mount(IndexPage, {
            store, localVue,
        });

        const vm = wrapper.vm; 


        
        const button = wrapper.find('#btnToggle button')

        await button.trigger('click');
        expect(vm.viewingState).toContain('scrolling')



    })

    it('pokemon found on search success', async () => {

        const wrapper = shallowMount(ShowPage, {
            store, localVue,
            mocks: {
                $route,
           
            },

        });
        const vm = wrapper.vm;

        await vm.callApiData()
        
        await flushPromises();

        const text = vm.notFound

        expect(text).toBe(false)

        
        
    })

    it('should go in pokemon show page when input has value and search button is pressed', async () => {
        const wrapper = mount(MainLayout, {
            mixins: [routeManage],
            stubs: ['router-view'],
            store, localVue,
            mocks: {
                $router,
                $route,
            },

        });
        const vm = wrapper.vm;


  
        await vm.onSearch()
        await flushPromises()
      
        expect($router.push).lastCalledWith('/pokemon/pikachu')
    })
    

    it('should go in home page when logo is pressed', async () => {
        const wrapper = mount(MainLayout, {
            mixins: [routeManage],
            stubs: ['router-view'],
            store, localVue,
            mocks: {
                $router,
                $route,
            },

        });



        wrapper.find('#logo').trigger('click')


        await flushPromises()

        expect($router.push).lastCalledWith('/')
    })    
})



